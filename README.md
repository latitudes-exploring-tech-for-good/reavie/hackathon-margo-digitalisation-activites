# Réavie
##### Défi de digitalisation des activités

À l'occasion du hackathon Tech for Good organisé par [Margo](https://www.margo-group.com/), [Latitudes](http://latitudes.cc/) et [makesense](https://makesense.org/), l'association [Réavie](http://asso-reavie.fr/) cherche à développer une application mobile pour réaliser le diagnostic ressources (document d'étude du potentiel de réemploi des matériaux avant de démarrer un chantier de rénovation ou démolition) et l’inventaire de matériaux en vue du passage sur les plateformes physique de réemploi de l’association.

##### #0 | Résultats obtenus lors du hackathon
Voici les livrables obtenus pour le défi proposé ci-dessous.

Équipe 1 
+ "Specs" de l'application : https://drive.google.com/a/latitudes.cc/file/d/1U6P213ZaUr4ysfKM5w_pzukk7GM-KsiX/view?usp=sharing
+ UI : https://drive.google.com/a/latitudes.cc/file/d/1U5GPwgBmhLV3dNNVF_PPMGZ-6lj0crbr/view?usp=sharing
+ Présentation : https://drive.google.com/a/latitudes.cc/file/d/1U1dR6R_RvXrSN0R5TFYrW9tCWZJhX26Q/view?usp=sharing

Équipe 2
+ https://gitlab.com/oubenal/hackathon-margo-digitalisation-activites/tree/master/BackEnd
+ Présentaiton : https://drive.google.com/a/latitudes.cc/file/d/1TpOZmy_GIACB-DwHuwjiamR6fsCSNY4x/view?usp=sharing


##### #1 | Présentation de Réavie

Créée en avril 2017, RéaVie est une association environnementale à vocation sociale qui participe à la transition du secteur du bâtiment vers une économie circulaire. Son action se construit autour de 3 piliers :
*   Développer le réemploi de matériaux du bâtiment afin de réduire les déchets du secteur ;
*   Former des personnes en insertion professionnelle aux métiers du réemploi et du bâtiment ;
*   Sensibiliser le grand public aux problématiques liées aux déchets et promouvoir le vivre ensemble autour des chantiers.

Le processus de collecte est décomposé en trois étapes :
*   **Diagnostic ressources (DR)** : relevé photographique, quantitatif et des caractéristiques des éléments qui présentent un potentiel de réemploi sur un chantier. Il doit être réalisé dès l’attribution du chantier à une entreprise, avant l’intervention des cureurs-démolisseurs. Le livrable est un document récapitulatif sous forme de tableau où chaque entrée présente un matériau et flèche un process de dépose et une filière de réemploi.   
*   **Dépose méthodique (DM)** : l’intervention pendant laquelle les équipes de RéaVie viennent déconstruire et désassembler les matériaux identifiés au préalable, elle a été préparée par le diagnostic ressource qui permet de programmer quels éléments vont être ciblés.
*   **Inventaire** : permet de répertorier tous les éléments présents sur les plateformes physiques de réemploi afin de pouvoir suivre au mieux les entrées et sorties de matériaux.

##### #2 | Problématique : digitalisation des activités
Réaliser une application mobile permettant à Réavie de renseigner directement des informations sur les matériaux récupérables sur les chantiers, puis d’agréger toutes ces données afin de faciliter la collecte et servir pour constituer l’inventaire des stocks de l’association.

> La problématique : faciliter l'étape la plus critique, qui est le maintien continu d’un inventaire à jour avec l’emplacement et l’état des matériaux, et générer des documents en PDF et Excel à partir des données récoltées.

##### #3 | Le défi proposé
La mission consiste à mettre en place un système informatique permettant, lors du diagnostic ressources, de saisir les matériaux et équipements en place, pour ensuite générer un catalogue de matériaux récupérables et aussi alimenter un inventaire sur l’état et la destination finale de ces matériaux.

##### #4 | Livrables
Le livrable devra être une application mobile permettant de faciliter au minimum le maintien de l'inventaire de l'association. Pour cela nous vous proposons (à titre indicatif) deux points de départ :
1.  Commencer par réaliser une fiche de prise de données pour la partie diagnostic ressources, et ensuite les enrichir à la phase de l’inventaire :
    *   Utiliser l’application pour remplir une fiche descriptive des matériaux destinés à être récupérés ultérieurement, en les regroupant sous un même projet.
    *   Pouvoir rééditer le même projet le jour de la DM (si l’état de certains matériaux a changé)
    *   Enrichir les informations collectées au moment de la DR et DM pour finaliser le processus et transférer toutes ces informations dans l’inventaire.
2.  Réaliser une application qui gère seulement le maintien de l’inventaire, et ensuite ajouter la possibilité de l’alimenter depuis une fiche destinée aux DR :
    *   Utiliser l’application pour remplir une fiche descriptive des matériaux déjà entreposés dans les entrepôts de RéaVie.
    *   Ajouter la possibilité de transférer des données en masse depuis une source externe (fichier csv)

Voici quelques exemples d'applications existantes:
*   [monstock](https://www.monstock.net/)
*   [stock controller](https://www.xnr-sisbi.com/stock-portal/)
 
##### #5 | Ressources à disposition pour résoudre le défi
Pour commencer vous aurez besoin d'avoir GIT sur votre pc :
*   [Install GIT tutorial](https://www.atlassian.com/git/tutorials/install-git)
*   Selon la technologie choisie pour développer le projet, nous pouvons vous fournir l'accès aux serveurs de Margo pour héberger l'application.

###### Exemples de documents utilisés
Voir le dossier _documents_ :
*   exemple de diagnostic ressources réalisé sur un chantier de l’association
*   inventaire des matériaux présents sur les plateformes de RéaVie

###### Frameworks utilisables pour réaliser une application mobile
*   [PhoneGap](https://phonegap.com/getstarted/)
*   [Apache Cordova](https://cordova.apache.org/#getstarted)
*   [ionic Framework](https://ionicframework.com/docs)
*   [Framework7](https://framework7.io/docs/introduction.html)

###### some useful links
*   Making a PWA with ionic : https://blog.ionicframework.com/how-to-make-pwas-with-ionic/
*   Talk on PWA (Progressive Web Apps) : https://youtu.be/2kv9tH_IYtQ

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Margo, Latitudes et makesense ont voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux associations, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux associations d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateurs, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les associations soutenues, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
Mohamed H. : Fondateur de Réavie

Abdallah A.M. : consultant Margo Consulting et en charge de la préparation du défi Réavie

Augustin C. : co-fondateur de Latitudes, et en charge de la préparation du défi Réavie



